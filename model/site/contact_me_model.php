<?php

class Contact_me_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    
    public function submit()
    {
        $output = "";
        
        if(isset($_POST['submit']))
        {
            $fname = escape(filterPOST('fname'));
            $lname = escape(filterPOST('lname'));
            $email = escape(filterPOST('email'));
            $subject = escape(filterPOST('subject'));
            $textarea = escape(filterPOST('textarea'));
            
            $output .= (empty(trim($fname))) ? sprintf("%s", "Firt name cannot be empty <br />") : "";
            
            $output .= (empty(trim($lname))) ? sprintf("%s", "Last name cannot be empty <br />") : "";
            
            $output .= (! preg_match("/^[\w\d._-]+?@[\w\d_.-]+?\.[a-z.]+?$/i", $email)) ? 
                            sprintf("%s", "The email is not valid <br />") : "";
            
            $output .= (empty(trim($subject))) ? sprintf("%s", "Email header is empty <br />") : "";
            
            $output .= (empty(trim($textarea))) ? sprintf("%s", "Email message is empty <br />") : "";
            
            if($output == "")
            {
                $to = 'robyntimjnr@yahoo.it';
                //mail($to, $subject, $textarea, 'From:' .$email);
                $output = sprintf("%s", "Message sent from Mr./Mss. {$fname} {$lname}");  
            } 
        }
        return $output;
    }
}
