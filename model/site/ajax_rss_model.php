<?php
/**
 * Description of ajax_rss_model
 *
 * @author Robert
 */
class Ajax_rss_model extends Model {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function ajaxNews($xml)
    {
        $xml = escape($xml);
        $stm = $this->getPDO()->prepare("SELECT `link` FROM `rss` WHERE `title`= :title");
        $stm->bindParam(':title', $xml);
        $stm->execute();
        
        if($xml == 'choose')
        {
            // Display that message if thee user does not select anything
            echo '<div id="rssChoose">'.'Please choose from one of the given RSS news feed'.'</div>';
        }
        else
        {
            if($stm->rowCount() == 0)
            {
                // Display message if user manipulate the GET variable form the URL 
                echo '<div id="rssChoose">'.'Sorry but the rss chosen is not present'.'</div>';
            }
            else
            {
                //fetches xml url if present in the table
                $rss = $stm->fetch(PDO::FETCH_OBJ);
                // Download xml file if title is present in the database
                $dom = simplexml_load_file($rss->link);
                echo "<h3 class='rssTitle'>" . htmlspecialchars($dom->channel->title) . "</h3>";
                echo '<ul>';
                foreach($dom->channel->item as $item)
                {
                    echo "<li class='item'>";
                    echo "<a class='item' href="."'". htmlspecialchars($item->link) ."'"." target='_blank' >";
                    echo htmlspecialchars($item->title);
                    echo"</a>";
                    echo "</li>";
                }
                echo '</ul>';
            }
        }
    }
}
