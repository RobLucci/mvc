<?php

class Index_model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public static function rss()
    {
        $stm = self::getPDO()->prepare("SELECT `title` FROM `rss`");
        $stm->execute(); 
        if($stm)
        {
            if($stm->rowCount() > 0 )
            {
                echo "<option selected='selected' value ='choose'>";
                echo "Select one newsFeed from...";
                echo "</option>";
                //Loop through rss titles
                $options = $stm->rowCount();
                for($j = 0 ; $j < $options; $j++)
                {
                    $option = $stm->fetch(PDO::FETCH_OBJ);
                    echo "<option value='$option->title'>";
                    echo $option->title;
                    echo "</option>";
                }
            }
            else
            {
                echo "<option selected = 'selected'>";
                echo "No RSS feeds at the moment";
                echo "</option>";
            }
        }
        else
        {
            echo "<option selected = 'selected'>";
            echo "Database error";
            echo "</option>";
        }
    }

    
    public static function getNews()
    {
        $db = new Database();   
        if(isset($_GET['submit']))
        {
            $title = escape(filterGET('xml'));
            
            $stm = $db->prepare("SELECT `link` FROM `rss` WHERE `title`= :title");
            $stm->bindParam(':title', $title);
            $stm->execute();

            if($title == 'choose')
            {
                // Display that message if thee user does not select anything
                echo '<div id="rssChoose">'.'Please choose from one of the given RSS news feed'.'</div>';
            }
            else
            {
                if($stm->rowCount() == 0)
                {
                    // Display message if user manipulate the GET variable form the URL 
                    echo '<div id="rssChoose">'.'Sorry but the rss chosen is not present'.'</div>';
                }
                else
                {
                    //fetches xml url if present in the table
                    $xml = $stm->fetch(PDO::FETCH_OBJ);
                    // Download xml file if title is present in the database
                    $dom = simplexml_load_file($xml->link);
                    echo '<div id="rss_links">';
                    echo "<h3 class='rssTitle'>" . $dom->channel->title . "</h3>";
                    echo '<ul>';
                    foreach($dom->channel->item as $item)
                    {
                        print "<li class='item'>";
                        print "<a class='item' href= '{$item->link}' target='_blank' >";
                        print $item->title;
                        print "</a>";
                        print "</li>";
                    }
                    echo '</ul>';
                    echo '</div>';
                }
            }
        }
    }
}

?>