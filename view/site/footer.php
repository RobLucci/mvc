
</div><!-- end of page or end of page2 for index view -->

<footer>

    <p class="footerpara">
        Copyright &#169; <?= date('Y'); ?> all rights reserved.
        No portion of robertdlaw.tk may be duplicated, redistributed or manipulated in any form.
        By accessing any information beyond this page, you agree to abide by this Privacy Policy.
    </p>
       
</footer> <!-- end of footer -->
<?php
//include script files dinamically
        if(isset($this->js))
        {
            foreach($this->js as $js)
            {
                echo '<script type="text/javascript" src="'. URL .'site/js/'. $js . '.js' .'"></script>';
            } 
        }
    ?>
</body> <!-- end of body -->

</html>
