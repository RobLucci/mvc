<div id="container">
    <div>
        <h2>Robert</h2>
        <p>This website has been built form the ground up, it is a result of my self-teaching of programming languages such as : HTML,CSS,JavaScript, PHP
        and Bash.
        This website is constantly in development as I learn new skill to improve performances and aesthetic.
        If you have any kind of advice I am more than willing to listen for them </p>
    </div>
    <div>
        <h2><a href="http://nti11407245.hosting.warkscol.ac.uk/" target="_blank">My Work</a></h2>

        <p>
            In this site you will see all my College work of the year 2013/14. It was my first web site ever created and I thought that it would be nice if
            I still keep it around.<br />
            Unfortunately the website is hosted by the College's servers but in the near future it will be transferred under my domain address, therefore it may
            be subjected to down times, which I have no control of.<br />
            Because of reason that I have yet to comprehend we had to stop using the website and move our work materials onto Google's site, that is why
            you will see actual work material only under the "Unit 1" link, the rest of the Units are dead ends links.
            If you want to see my full work you need to access this
            <a href="https://sites.google.com/a/stu.warwickshire.ac.uk/disappointing/" target="_blank">web page</a> <i>(link opens a new tab)</i>
        </p>
    </div>
</div><!-- end of container div -->
        
