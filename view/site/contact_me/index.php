
<!-- Email form for visitors-->

<form action="<?= URL; ?>contact_me/submit" id="mail_form" method="POST">
    <?php if(isset($this->output)) echo $this->output;?>
        <table class="mail">
            <thead>
                <tr>
                    <th colspan="2">
                        EMAIL FORM
                    </th> 
                </tr>
            </thead>
            <tbody>
                <tr class="odd">
                    <td>
                        <label for="first_name"></label>
                        <p> Sender First Name:
                                <input type="text" value="<?php saveUserInput('fname'); ?>" id="first_name" name="fname" class="validate" size="30"/>
                            </p>
                    </td>
                </tr>
                <tr class="even">
                    <td>
                        <label for="last_name"></label>
                        <p> Sender Last Name:
                                <input type="text" value="<?php saveUserInput('lname'); ?>" id="last_name" name="lname" class="validate" size="30"/>
                            </p>
                    </td>

                </tr>
                <tr class="odd">
                    <td>
                        <label for="email_address"></label>
                        <p> Your E-mail:
                                <input type="email" value="<?php saveUserInput('email'); ?>" id="email_addr" name="email" class="validate" size="30"/>
                            </p>
                    </td>
                </tr>
                <tr class="even">
                    <td>
                        <label for="subj"></label>
                            <p> Subject:
                                <input type="text" value="<?php saveUserInput('subject'); ?>" id="subj" name="subject" class="validate" size="30"/>
                            </p>
                    </td>
                </tr>
                <tr class="odd">
                    <td>
                        <label for="text"> Text: </label>
                        <p>
                            <textarea name="textarea" id="text" class="validate">
                                <?php saveUserInput('textarea'); ?>
                            </textarea>
                        </p>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr class="even">
                    <td>
                        <input type="submit" id="send" name="submit" value="Send"/>
                    </td>
                    <td>
                        <input type="reset" id="reset" name="reset" value="Reset"/>
                    </td>
                </tr>
            </tfoot>
        </table>
    </form>

