<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        HEADER
    </title>
    
    <meta charset="UTF-8" />

    <link href="<?= URL_SITE; ?>css/styles.css" style="text/css" rel="stylesheet"/>

    <!-- Google analytic script code -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-48755887-1', 'robertdlaw.tk');
        ga('send', 'pageview');

    </script>
</head>
<body>

<div id="page">

    <header>

        <h1 class="title"> HEADER </h1>

        <a href="<?= URL; ?>index"><img src="../images/logo.png" alt="Logo of the site" class="head_img"/></a>

        <nav>
            <ul>
                <li><a href="<?= URL; ?>index"> Home </a></li>
                <li><a href="robert"> Robert </a><span class="darrow">&#9660;</span>
                    <ul class="subnav">
                        <li><a href="http://nti11407245.hosting.warkscol.ac.uk/" target="_blank" title="Opens new tab" > My work </a></li>
                    </ul>
                </li>
                <li><a href="nestim"> NesTim </a></li>
                <li><a href="<?= URL; ?>contact_me"> Contact me </a></li>
                <li><a href="<?= URL; ?>extra"> Extra </a><span class="darrow">&#9660;</span>
                    <ul class="subnav">
                        <li><a href="cv"> Curriculum </a></li>
                        <li><a href="cleveland"> Fort William </a></li>
                        <li><a href="online_guide?p=1"> My program </a></li>
                    </ul><!-- end of subnav unordered list -->
                </li>
                <li><a href="<?= URL; ?>admin"> Admin area </a><span class="darrow">&#9660;</span>
                    <ul class="subnav">
                        <li><a href="#"> Mini-apps </a><span class="rarrow">&#9654;</span>
                            <ul class="subnav2">
                                <li><a href="customer_form"> Customer Form </a></li>
                                <li><a href="equipment_form"> Equip. Form </a></li>
                            </ul><!-- end of subnav2 unordered list -->
                        </li>
                    </ul><!-- end of subnav unordered list -->
                </li>
            </ul><!-- end of unordered list -->
        </nav> <!-- end of nav -->
    </header> <!-- end of header -->

