<?php

// filtering the superglobal POST
function filterPOST($key)
{
    return filter_input(INPUT_POST,$key,FILTER_SANITIZE_STRING);
}

// filtering the superglobal SERVER
function filterSERVER($key)
{
    return filter_input(INPUT_SERVER,$key,FILTER_SANITIZE_STRING);
}

// filtering the superglobal GET
function filterGET($key)
{
    return filter_input(INPUT_GET,$key,FILTER_SANITIZE_STRING);
}

function escape($string)
{
    $string = strip_tags($string);
    $string = htmlspecialchars($string, ENT_QUOTES, "UTF-8");
    return stripslashes($string);
}

// used for form validation 
function saveUserInput($key)
{
    if(isset($_POST[$key])) 
    {
       echo escape(filterPOST($key));
    }
}

function reroute($page)
{
    $host = escape(filterSERVER('HTTP_HOST'));
    //$path = rtrim(dirname($host));
    //echo filterSERVER('PHP_SELF');
    echo "http://$host/$page";
    header("Location: http://$host/$page");
}

?>