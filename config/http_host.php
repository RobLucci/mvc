<?php
require_once '../helper/site/helper.php'; 

define('URL', 'http://'. filterSERVER("HTTP_HOST") .'/');

// absolute path for the site files
define('URL_SITE','http://'. filterSERVER("HTTP_HOST") .'/site/');

//absolute path for the social network files
define('URL_SN','http://'. filterSERVER("HTTP_HOST") .'/sn/');

?>