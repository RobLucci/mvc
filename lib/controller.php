<?php

// Parent controller where every controller is child of

class Controller
{
    private $view, // var <object> view object
            $model; // var <object> model object 


    public function __construct() 
    {
        //echo 'I am the parent controller <br />';
        $this->view = new View();
        
    }

    public function loadModel($name)
    {
        $pathModel = '../model/site/'. $name . '_model.php';
        $snPathModel = '../model/sn/'. $name . '_model.php';

        if (file_exists($pathModel)) {
            $this->initModelObj($name, $pathModel);
        }elseif(file_exists($snPathModel)){
            $this->initModelObj($name, $snPathModel);
        }else{
            echo 'I DON\'T HAVE A MODEL !!! <br />';
        }
    }
    
    public function initModelObj($modelName, $file)
    {
        require $file;
        $modelName .= '_model';
        $this->model = new $modelName();
    }

    //GETTERS - return value of private properties

    // getter - retunr the value of property view
    public function getView()
    {
        return $this->view;
    }
    
    public function getModel()
    {
        return $this->model;
    }
}