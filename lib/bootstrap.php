<?php

class Bootstrap
{
    private $controller; //var <obj> controller object

    public function __construct()
    {
        // get url
        $url = isset($_GET['url']) ? escape($_GET['url']) : 'index';

        // right trim of slashes
        $url = rtrim($url, '/');

        // explode url parameter
        $url = explode('/', $url);

        //print_r($url);

        $sitePath = '../controller/site/' . $url[0] . '.php'; //website controllers
        $snPath = '../controller/sn/' . $url[0] . '.php'; //social network controllers 

        if (file_exists($sitePath)) {
            
            $this->initContollerObj($url[0], $sitePath);
        } elseif (file_exists($snPath)) {
                $this->initContollerObj($url[0], $snPath);
            } else {
                $this->error($url);
            }

            if (isset($url[1])) {
                if (method_exists($this->controller, $url[1])) {
                    if (isset($url[2])) {
                        $this->controller->{$url[1]}($url[2]);
                    } else {
                        $this->controller->{$url[1]}();
                    }
                } else {
                    //print_r($url);     
                    $this->error($url);
                }
            }
    }
    
    public function initContollerObj($page, $file)
    {
        require_once $file;
        $this->controller = new $page();
        $this->controller->loadModel($page);
        $this->controller->htmlBody();
    }

    public function error($url)
    {
        require '../controller/site/error.php';
        $this->controller = new Error();
        $this->controller->loadModel('Error');
        $this->controller->htmlBody();
        $this->controller->errorMsg($url);
        return false;
    }
    
    
    //METHOD NOT USED AT THE MOMENT
    public function getController()
    {
        return $this->controller;
    }
    
    public function __destruct()        
    {
    }
}