<?php

class Model
{
    private static $PDO = null; // var <obj> database obj
     
    public function __construct()
    {
        $this->setPDO();
        //self::$PDO = new Database();
    }
    
    private function setPDO()
    {
        if(!isset(self::$PDO))
        {
            self::$PDO = new Database;
        }
    }
    
    public function getPDO()
    {
        return self::$PDO;
    }
}

?>