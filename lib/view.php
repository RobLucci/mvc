<?php

class View
{
    public $js = array();
    public $eMsg = "";
    public $output;
    
    public function __construct()
    {
        //echo 'I am the view controler <br />';
    }


    public function render($name, $renderOff = null)
    {
        $body = '../view/site/' . $name . '.php';
        $header = '../view/site/header.php';
        $footer = '../view/site/footer.php';
        
        $snBody = '../view/sn/' . $name . '.php';
        $snHeader = '../view/sn/header.php';
        $snFooter = '../view/sn/footer.php';

        if (file_exists($body) && file_exists($header) && file_exists($footer)) {
            if (isset($renderOff)) {
                require_once $body;
            } else {
                require_once $header;
                require_once $body;
                require_once $footer;
            }
        }elseif (file_exists($snBody) && file_exists($snHeader) && file_exists($snFooter)) {
            if (isset($renderOff)) {
                require_once $snBody;
            } else {
                require_once $snHeader;
                require_once $snBody;
                require_once $snFooter;
            }
        }
    }
    
    public function getJs()
    {
        return $this->js;
    }
}