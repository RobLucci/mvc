<?php

// insert auto loader
//require_once '../lib/bootstrap.php';
//require_once '../lib/controller.php';
//require_once '../lib/view.php';
//require_once '../lib/model.php';
//require_once '../lib/database.php';


require_once '../config/http_host.php';
require_once '../config/database.php';

spl_autoload_register(function($class) {
    $class = strtolower($class);
    require_once '../lib/'. $class . '.php';
});

$html = new Bootstrap();
?>
