<?php

if(isset($_GET['submit']) && isset($_GET['xml']))
{
    $title = $_GET['xml'];
    echo $title;
    //news($title);
}

function news($title)
{
    $stm = $this->getPDO()->prepare("SELECT `link` FROM `rss` WHERE `title`= :title");
    $stm->bindParam(':title', $title);
    $stm->execute();
    
    if($title == 'choose')
    {
        // Display that message if thee user does not select anything
        //echo '<div id="rssChoose">'.'Please choose from one of the given RSS news feed'.'</div>';
    }
    else
    {
        if($stm->rowCount() == 0)
        {
            // Display message if user manipulate the GET variable form the URL 
            //echo '<div id="rssChoose">'.'Sorry but the rss chosen is not present'.'</div>';
        }
        else
        {
            //fetches xml url if present in the table
            $xml = $stm->fetch(PDO::FETCH_OBJ);
            // Download xml file if title is present in the database
            $dom = simplexml_load_file($xml->link);
            echo "<h3 class='rssTitle'>" . $dom->channel->title . "</h3>";
            echo '<ul>';
            foreach($dom->channel->item as $item)
            {
                print "<li class='item'>";
                print "<a class='item' href= '{$item->link}' target='_blank' >";
                print $item->title;
                print "</a>";
                print "</li>";
            }
            echo '</ul>';
        }
    }
}