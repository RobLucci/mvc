// return an object
function O(obj)
{
    if(typeof obj === 'object')
    {
        return obj;
    }
    else
    {
        return document.getElementById(obj);
    }
}

//style an object
function S(obj)
{
    return O(obj).style;
}

//return elements with the same class attribute value into an array 
function C(name)
{
    var element = document.getElementByTagName('*');
    var objects = [];
    
    for( var j = 0; j < element.length; ++j)
    {
        if(element[j].className === name)
        {
            objects.push(element[j]);
        }
    }
    
    return objects;
}