function displayRss()
{
    var rss = O("select").value;
    var noCached = "?nocached=" + Math.random() * 10000;
    var request = ajaxRequest();
    request.open("GET", "ajax_rss/ajaxNews/" + rss + noCached, true);
    //request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    request.send(null);
    
    request.onreadystatechange = function()
    {
        if(this.readyState == 4)
        {
            if(this.status == 200)
            {
                if(this.responseText != null)
                {
                    if(O('rss_links')) //Check if element is in DOM
                    {
                      O('page2').removeChild(O('rss_links')); //Remove element from the DOM 
                    }
                    var parentEl = O('page2'); //Target parent element
                    var el = document.createElement('div'); //Create div element
                    el.id = 'rss_links'; // Assign id attribute to the Div element 
                    parentEl.appendChild(el); //Append div element into the parent element
                    var text = O('rss_links'); //Target div element
                    text.innerHTML = this.responseText; //Insert server repsonce into the Div element
                }
                else
                {
                    alert("Ajax error: No data received");
                }
            }
            else
            {
                alter("Ajax error: "+ this.statusText);
            }
        }
    }
}

window.onload = function()
{
    if(O('select'))
    {
        var el = O('select');
    el.addEventListener("change", displayRss, false);
    }
}

    