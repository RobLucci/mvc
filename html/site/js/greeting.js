/*Script for greeting the user based on the time*/
var today = new Date(); //create a new Date object
var hourNow = today.getHours(); // find the current hour
var greeting;

if (hourNow > 17)
{
    greeting = 'Good Evening!';
}
else if (hourNow >11)
{
    greeting = 'Good Afternoon!';
}
else if (hourNow > 5)
{
    greeting = 'Good Morning!';
}
else
{
    greeting = 'Welcome!';
}

document.write('<h3 class="js">' + greeting + '</h3>');