var elForm = O('mail_form');
elForm.addEventListener('submit', function(e){
    var fn = O('first_name').value.trim(); 
    var ln = O('last_name').value.trim(); 
    var em = O('email_addr').value.trim(); 
    var subj = O('subj').value.trim(); 
    var text = O('text').value.trim();
    validateSubmit(e, fn, ln, em, subj, text);}, false);


function validateSubmit(e, fn, ln, em, subj, msg)
{
    var result = "";
    result += validateFirstName(fn);
    result += validateLastName(ln);
    result += validateEmail(em);
    result += validateSubject(subj);
    result += validateText(msg);
    
    if(!result == "")
    {
        if(O("msg"))
        {
            O("mail_form").removeChild(O("msg"));
        }
        var parentEl = document.getElementById('mail_form');
        var el = document.createElement('p');
        el.id= "msg";
        var text =  document.createTextNode(result);
        el.appendChild(text);
        parentEl.insertBefore(el, parentEl.firstChild);
        e.preventDefault();
        return false;
    }
}

function validateFirstName(input)
{
    return (input.length == 0) ? "First name is empty |" : "";
}

function validateLastName(input)
{
    return (input.length == 0) ? " Last name is empty |" : "";  
}

function validateSubject(input)
{
    return (input.length == 0) ? " Subject is empty |" : "";
}

function validateEmail(input)
{
    return (!/^[a-zA-Z0-9_.-]+?@[a-zA-Z0-9_.-]+?\.[a-z]+?$/.test(input)) ? " Email is not valid |" : "";

}

function validateText(input)
{
    return (input.length == 0) ? " Text is empty" : "";  
}



