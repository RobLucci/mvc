<?php

/**
 * Description of ajax_rss
 *
 * @author Robert
 */
class Ajax_rss extends Controller {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function htmlBody()
    {
        //Body not needed
    }
    
    public function ajaxNews($xml)
    {
        $this->getModel()->ajaxNews($xml);
    }
}
