<?php

class Contact_me extends Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->getView()->js = array( 'OSC', 'validation_form');
    }
    
    public function htmlBody()
    {
        $this->getView()->output= $this->submit();
        $this->getView()->render('contact_me/index');
    }
    
    public function submit()
    {
        return $this->getModel()->submit();
    }
}

?>