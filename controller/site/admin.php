<?php
class Admin extends Controller {
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function htmlBody()
    {
        $this->getView()->render('admin/index');
    }
}
