<?php

class Extra extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function htmlBody()
    {
        $this->getView()->render('extra/index');
    }
}

?>
