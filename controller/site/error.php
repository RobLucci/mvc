<?php

class Error extends Controller
{
    public function __construct()
    {
        parent::__construct();
        echo 'This page does not exist <br />';
        $this->getView()->eMsg;
    }
    
    public function htmlBody()
    {
        $this->getView()->render('error/index', 1);
    }
    
    public function errorMsg($array)
    {
        $this->getModel()->errorMsg($array);
    }
    
    public function __destruct()        
    {
    }
}



?>