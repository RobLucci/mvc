<?php

class Index extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->getView()->js= array('OSC','rss','ajax_request');
        //echo 'I am the index controller <br />'; 
    }        
    
    public function htmlBody()
    {
        $this->getView()->render('index/index');
    }
    
    public function getNews()
    {
        //NOTE: Static function from the index model
    }


    public function test($arg = false)
    {
        echo 'I am the Index controller method test <br />';
        echo 'Optional argument: '. $arg ;
    } 
}

?>